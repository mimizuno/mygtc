package main

import (
	"fmt"
	"os"

	"github.com/koron/gtc/gtcore"
)

func main() {
	catalog := gtcore.DefaultCatalog.Merge([]gtcore.Tool{
		{
			Path: "github.com/mimizuno/mygtc",
			Desc: "My own go tools catalog",
		},
		{Path: "github.com/mattn/memo"},
		{Path: "github.com/motemen/ghq"},
		{Path: "github.com/peco/peco"},
		{Path: "github.com/monochromegane/the_platinum_searcher/cmd/pt"},
		{Path: "github.com/derekparker/delve/cmd/dlv"},
		{Path: "github.com/jteeuwen/go-bindata/go-bindata"},
		{Path: "github.com/mattn/sudo"},
		{Path: "github.com/awalterschulze/goderive"},
	}...)
	err := catalog.Run(os.Args)
	if err != nil {
		fmt.Println(err.Error())
	}
}
